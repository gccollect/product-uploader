﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ProductUploader.Cli.Core.Contexts;
using ProductUploader.Cli.Core.Exceptions;
using ProductUploader.Cli.Core.Operations;
using ProductUploader.Cli.Core.Readers;
using ProductUploader.Cli.Core.Writers;

namespace ProductUploader.Cli
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection
                .AddSingleton<IImportOperation, ImportOperation>()
                .AddSingleton<IWriter, ConsoleWriter>()
                .AddSingleton<IReaderFactory, ReaderFactory>();

            ServiceProvider buildServiceProvider = serviceCollection.BuildServiceProvider();

            try
            {
                var importOperation = buildServiceProvider.GetRequiredService<IImportOperation>();

                if (args.Length < 2)
                {
                    throw new ArgumentParameterMissingException("You have to enter brand and source parameter !");
                }

                string brand = args[0];
                string source = args[1];

                var importOperationContext = new ImportOperationContext(brand, source);

                await importOperation.PerformAsync(importOperationContext);
            }
            catch (Exception e)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(e.InnerException?.Message ?? e.Message);
                Console.BackgroundColor = ConsoleColor.Black;
                Environment.Exit(1);
                return;
            }

            Environment.Exit(0);
        }
    }
}
