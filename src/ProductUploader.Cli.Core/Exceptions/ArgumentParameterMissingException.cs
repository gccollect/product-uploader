﻿using System;

namespace ProductUploader.Cli.Core.Exceptions
{
    public class ArgumentParameterMissingException : Exception
    {
        public ArgumentParameterMissingException(string message) : base(message)
        {
        }
    }
}