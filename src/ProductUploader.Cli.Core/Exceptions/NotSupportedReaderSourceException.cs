﻿using System;

namespace ProductUploader.Cli.Core.Exceptions
{
    public class NotSupportedReaderSourceException : Exception
    {
        public NotSupportedReaderSourceException(string message) : base(message)
        {
        }
    }
}