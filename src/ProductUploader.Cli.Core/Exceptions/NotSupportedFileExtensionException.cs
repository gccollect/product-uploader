﻿using System;

namespace ProductUploader.Cli.Core.Exceptions
{
    public class NotSupportedFileExtensionException : Exception
    {
        public NotSupportedFileExtensionException(string message) : base(message)
        {
        }
    }
}