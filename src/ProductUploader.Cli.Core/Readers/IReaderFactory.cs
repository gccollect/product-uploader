﻿namespace ProductUploader.Cli.Core.Readers
{
    public interface IReaderFactory
    {
        IReader GetReader(string source);
    }
}