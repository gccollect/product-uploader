﻿using System;
using ProductUploader.Cli.Core.Exceptions;

namespace ProductUploader.Cli.Core.Readers
{
    public class ReaderFactory : IReaderFactory
    {
        public IReader GetReader(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException("Source should not be empty", nameof(source));
            }

            bool isFilePath = source.StartsWith(Constants.DirectoryName);

            if (!isFilePath)
            {
                throw new NotSupportedReaderSourceException($"It can only read file from under {Constants.DirectoryName} directory");
            }

            return new FileReader();
        }
    }
}