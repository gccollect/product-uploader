﻿using System.IO;
using System.Threading.Tasks;
using ProductUploader.Cli.Core.Exceptions;
using ProductUploader.Cli.Core.Parser;

namespace ProductUploader.Cli.Core.Readers
{
    public class FileReader : IReader
    {
        public async Task<T> ReadAsync<T>(string path)
        {
            var extension = Path.GetExtension(path);

            IParser parser;
            switch (extension)
            {
                case ".json":
                    parser = new JsonParser();
                    break;
                case ".yaml":
                    parser = new YamlParser();
                    break;
                default:
                    throw new NotSupportedFileExtensionException("Not supported file extension");
            }

            var exists = File.Exists(path);

            if (!exists)
            {
                throw new FileNotFoundException($"{path} not found !");
            }

            using (var sr = new StreamReader(path))
            {
                var data = await sr.ReadToEndAsync();
                return parser.Parse<T>(data);
            }
        }
    }
}