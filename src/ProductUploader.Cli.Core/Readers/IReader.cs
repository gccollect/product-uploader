﻿using System.Threading.Tasks;

namespace ProductUploader.Cli.Core.Readers
{
    public interface IReader
    {
        Task<T> ReadAsync<T>(string source);
    }
}