﻿namespace ProductUploader.Cli.Core.Models
{
    public class Product
    {
        public string Name { get; set; }

        public string[] Categories { get; set; }

        public string Twitter { get; set; }
    }
}