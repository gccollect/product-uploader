﻿namespace ProductUploader.Cli.Core.Models
{
    public class CapterraProduct
    {
        public string Tags { get; set; }

        public string Name { get; set; }

        public string Twitter { get; set; }
    }
}