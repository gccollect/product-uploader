﻿namespace ProductUploader.Cli.Core.Models
{
    public class SoftwareAdviceProductList
    {
        public SoftwareAdviceProduct[] Products { get; set; }
    }

    public class SoftwareAdviceProduct
    {
        public string[] Categories { get; set; }

        public string Twitter { get; set; }

        public string Title { get; set; }
    }
}