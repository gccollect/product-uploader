﻿using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ProductUploader.Cli.Core.Parser
{
    public class YamlParser : IParser
    {
        public T Parse<T>(string text)
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .Build();

            var result = deserializer.Deserialize<T>(text);

            return result;
        }
    }
}