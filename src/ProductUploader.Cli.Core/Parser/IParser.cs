﻿namespace ProductUploader.Cli.Core.Parser
{
    public interface IParser
    {
        T Parse<T>(string text);
    }
}