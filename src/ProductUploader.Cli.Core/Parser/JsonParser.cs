﻿using System.Text.Json;

namespace ProductUploader.Cli.Core.Parser
{
    public class JsonParser : IParser
    {
        public T Parse<T>(string text)
        {
            var jsonSerializerOptions = new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            T result = JsonSerializer.Deserialize<T>(text, jsonSerializerOptions);

            return result;
        }
    }
}