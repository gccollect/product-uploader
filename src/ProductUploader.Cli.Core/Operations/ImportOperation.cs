﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductUploader.Cli.Core.Contexts;
using ProductUploader.Cli.Core.Enums;
using ProductUploader.Cli.Core.Models;
using ProductUploader.Cli.Core.Readers;
using ProductUploader.Cli.Core.Writers;

namespace ProductUploader.Cli.Core.Operations
{
    public class ImportOperation : IImportOperation
    {
        private readonly IReaderFactory _readerFactory;
        private readonly IWriter _writer;

        public ImportOperation(IReaderFactory readerFactory, IWriter writer)
        {
            _readerFactory = readerFactory;
            _writer = writer;
        }

        public async Task PerformAsync(ImportOperationContext importOperationContext)
        {
            if (string.IsNullOrWhiteSpace(importOperationContext.Brand))
            {
                throw new ArgumentException("Cannot be empty", nameof(importOperationContext.Brand));
            }

            if (string.IsNullOrWhiteSpace(importOperationContext.Source))
            {
                throw new ArgumentException("Cannot be empty", nameof(importOperationContext.Source));
            }

            if (!Brands.IsIn(importOperationContext.Brand))
            {
                throw new ArgumentException($"Not supported brand !", nameof(importOperationContext.Brand));
            }

            var reader = _readerFactory.GetReader(importOperationContext.Source);

            var result = await ReadByBrand(importOperationContext.Brand, importOperationContext.Source, reader);

            _writer.WriteTo(result?.ToArray());
        }


        private async Task<List<Product>> ReadByBrand(string brand, string source, IReader reader)
        {
            if (Brands.Capterra.Name.ToLowerInvariant() == brand)
            {
                var capterraProducts = await reader.ReadAsync<CapterraProduct[]>(source);

                return capterraProducts.Select(m => new Product
                {
                    Name = m.Name,
                    Twitter = m.Twitter,
                    Categories = m.Tags.Split(",")
                }).ToList();
            }

            if (Brands.SoftwareAdvice.Name.ToLowerInvariant() == brand)
            {
                SoftwareAdviceProductList softwareAdviceProductLists = await reader.ReadAsync<SoftwareAdviceProductList>(source);

                return softwareAdviceProductLists.Products.Select(m => new Product
                {
                    Name = m.Title,
                    Twitter = m.Twitter,
                    Categories = m.Categories
                }).ToList();
            }

            return default;
        }
    }
}