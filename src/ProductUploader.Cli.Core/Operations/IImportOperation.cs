﻿using System.Threading.Tasks;
using ProductUploader.Cli.Core.Contexts;

namespace ProductUploader.Cli.Core.Operations
{
    public interface IImportOperation
    {
        Task PerformAsync(ImportOperationContext importOperationContext);
    }
}