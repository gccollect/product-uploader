﻿using System;
using ProductUploader.Cli.Core.Models;

namespace ProductUploader.Cli.Core.Writers
{
    public class ConsoleWriter : IWriter
    {
        public void WriteTo(Product[] products)
        {
            foreach (var product in products)
            {
                string text = $"importing: Name: \"{product.Name}\";  Categories: {string.Join(",", product.Categories)}; Twitter: {product.Twitter}";

                Console.WriteLine(text);
            }
        }
    }
}