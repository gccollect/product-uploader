﻿using ProductUploader.Cli.Core.Models;

namespace ProductUploader.Cli.Core.Writers
{
    public interface IWriter
    {
        void WriteTo(Product[] product);
    }
}