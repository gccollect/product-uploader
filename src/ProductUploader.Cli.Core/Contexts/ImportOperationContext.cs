﻿namespace ProductUploader.Cli.Core.Contexts
{
    public class ImportOperationContext
    {
        public string Brand { get; }

        public string Source { get; }

        public ImportOperationContext(string brand, string source)
        {
            Brand = brand;
            Source = source;
        }
    }
}