﻿using System;
using System.Linq;

namespace ProductUploader.Cli.Core.Enums
{
    public sealed class Brands
    {
        public static readonly Brands Capterra = new Brands("Capterra");

        public static readonly Brands SoftwareAdvice = new Brands("SoftwareAdvice");

        public static readonly Brands[] BrandList =
        {
            Capterra, SoftwareAdvice
        };

        private Brands(string name)
        {
            Name = name;
        }

        public static bool IsIn(string name) => BrandList.Any(m => string.Equals(m.Name, name, StringComparison.OrdinalIgnoreCase));

        public string Name { get; }
    }
}