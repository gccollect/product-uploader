#!/usr/bin/env bash

echo $(bash --version 2>&1 | head -n 1)

set -eo pipefail
SCRIPT_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)

BUILD_PROJECT_FILE="$SCRIPT_DIR/build/_build.csproj"

echo "Microsoft (R) .NET Core SDK version $("$DOTNET_EXE" --version)"

dotnet build "$BUILD_PROJECT_FILE" /nodeReuse:false -nologo -clp:NoSummary --verbosity quiet
dotnet run --project "$BUILD_PROJECT_FILE" --no-build -- "$@"