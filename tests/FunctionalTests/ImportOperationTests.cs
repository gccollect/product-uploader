using System.Diagnostics;
using System.IO;
using Xunit;

namespace FunctionalTests
{
    public class ImportOperationTests
    {
        private static readonly string ExecutablePath =
            Path.Join(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\..\\..\\..\\")),
                "artifacts", "import");

        [Fact]
        public void ProductUploader_Should_Exit_With_0()
        {
            var process = Process.Start(ExecutablePath, "capterra feed-products/capterra.yaml");
            process.WaitForExit();

            int result = process.ExitCode;

            Assert.Equal(0, result);
        }

        [Fact]
        public void ProductUploader_Should_Exit_With_1()
        {
            var process = Process.Start(ExecutablePath, "capterra1 feed-products/capterra.yaml");
            process.WaitForExit();

            int result = process.ExitCode;

            Assert.Equal(1, result);
        }
    }
}
