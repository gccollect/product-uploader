﻿using System;
using System.Threading.Tasks;
using Moq;
using ProductUploader.Cli.Core.Contexts;
using ProductUploader.Cli.Core.Models;
using ProductUploader.Cli.Core.Operations;
using ProductUploader.Cli.Core.Readers;
using ProductUploader.Cli.Core.Writers;
using Xunit;

namespace UnitTests
{
    public class ImportOperationTests
    {
        private readonly Mock<IReaderFactory> _readerFactoryMock;
        private readonly Mock<IWriter> _writerMock;

        public ImportOperationTests()
        {
            _readerFactoryMock = new Mock<IReaderFactory>(MockBehavior.Strict);
            _writerMock = new Mock<IWriter>(MockBehavior.Strict);
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("capterra", "")]
        [InlineData("", "feed-products/test.yaml")]
        [InlineData(null, "feed-products/test.yaml")]
        [InlineData(null, null)]
        public async Task PerformAsync_Should_Throw_Argument_Exception_If_Brand_Or_Source_Empty_Or_Null(string brand, string source)
        {
            var importOperationContext = new ImportOperationContext(brand, source);

            var classUnderTest = new ImportOperation(_readerFactoryMock.Object, _writerMock.Object);

            await Assert.ThrowsAsync<ArgumentException>(() => classUnderTest.PerformAsync(importOperationContext));
        }

        [Fact]
        public async Task PerformAsync_Should_Throw_Argument_Exception_If_Brand_Is_Not_Supported()
        {
            var importOperationContext = new ImportOperationContext("brand", "capterra");

            var classUnderTest = new ImportOperation(_readerFactoryMock.Object, _writerMock.Object);

            var argumentException = await Assert.ThrowsAsync<ArgumentException>(() => classUnderTest.PerformAsync(importOperationContext));

            Assert.Equal("Brand", argumentException.ParamName);
            Assert.Equal("Not supported brand ! (Parameter 'Brand')", argumentException.Message);
        }

        [Fact]
        public async Task PerformAsync_Should_Call_Writer()
        {
            var importOperationContext = new ImportOperationContext("capterra", "feed-products/capterra.yaml");

            _readerFactoryMock.Setup(m => m.GetReader(importOperationContext.Source)).Returns(new FileReader());
            _writerMock.Setup(m => m.WriteTo(It.IsAny<Product[]>()));

            var classUnderTest = new ImportOperation(_readerFactoryMock.Object, _writerMock.Object);

            await classUnderTest.PerformAsync(importOperationContext);

            _readerFactoryMock.Verify(m => m.GetReader(importOperationContext.Source), Times.Once);
            _writerMock.Verify(m => m.WriteTo(It.IsAny<Product[]>()), Times.Once());
        }
    }
}