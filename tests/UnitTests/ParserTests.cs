﻿using ProductUploader.Cli.Core.Models;
using ProductUploader.Cli.Core.Parser;
using Xunit;

namespace UnitTests
{
    public class ParserTests
    {
        [Fact]
        public void JsonParser_Should_Deserialize_Json()
        {
            string json = @"{
        ""products"": [
            {
                ""categories"": [
                    ""Customer Service"",
                    ""Call Center""
                ],
                ""twitter"": ""@freshdesk"",
                ""title"": ""Freshdesk""
            },
            {
                ""categories"": [
                    ""CRM"",
                    ""Sales Management""
                ],
                ""title"": ""Zoho""
            }
        ]
    }";

            var jsonParser = new JsonParser();
            var softwareAdviceProductList = jsonParser.Parse<SoftwareAdviceProductList>(json);

            Assert.NotNull(softwareAdviceProductList);
            Assert.NotNull(softwareAdviceProductList.Products);
            Assert.Equal(2, softwareAdviceProductList.Products.Length);
        }

        [Fact]
        public void YamlParser_Should_Deserialize_Json()
        {
            string json = @"
-
  tags: ""Bugs & Issue Tracking,Development Tools""
  name: ""GitGHub""
  twitter: ""github""
-
  tags: ""Instant Messaging & Chat,Web Collaboration,Productivity""
  name: ""Slack""
  twitter: ""slackhq""
-
  tags: ""Project Management,Project Collaboration,Development Tools""
  name: ""JIRA Software""
  twitter: ""jira""
";

            var jsonParser = new YamlParser();
            var capterraProducts = jsonParser.Parse<CapterraProduct[]>(json);

            Assert.NotNull(capterraProducts);
            Assert.Equal(3, capterraProducts.Length);
        }
    }
}