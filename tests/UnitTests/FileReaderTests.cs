﻿using System.IO;
using System.Threading.Tasks;
using ProductUploader.Cli.Core.Exceptions;
using ProductUploader.Cli.Core.Models;
using ProductUploader.Cli.Core.Readers;
using Xunit;

namespace UnitTests
{
    public class FileReaderTests
    {
        [Theory]
        [InlineData("test.jpeg")]
        [InlineData("folder/test.csv")]
        public async Task ReadAsync_Should_Throw_NotSupportedFileExtensionException(string path)
        {
            var fileReader = new FileReader();

            await Assert.ThrowsAsync<NotSupportedFileExtensionException>(() => fileReader.ReadAsync<dynamic>(path));
        }

        [Theory]
        [InlineData("test.json")]
        [InlineData("folder/test.yaml")]
        public async Task ReadAsync_Should_Throw_FileNotFoundException_If_File_Does_Not_Exists(string path)
        {
            var fileReader = new FileReader();

            await Assert.ThrowsAsync<FileNotFoundException>(() => fileReader.ReadAsync<dynamic>(path));
        }

        
        [Fact]
        public async Task ReadAsync_Should_Return_ProductList()
        {
            string path = "feed-products/capterra.yaml";
            var fileReader = new FileReader();
            var capterraProduct = await fileReader.ReadAsync<CapterraProduct[]>(path);

            Assert.NotNull(capterraProduct);
            Assert.Equal(3, capterraProduct.Length);
        }
    }
}