﻿using System;
using ProductUploader.Cli.Core;
using ProductUploader.Cli.Core.Exceptions;
using ProductUploader.Cli.Core.Readers;
using Xunit;

namespace UnitTests
{
    public class ReaderFactoryTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void GetReader_Should_Throw_ArgumentException_When_Source_Is_Empty_Or_Null(string source)
        {
            var readerFactory = new ReaderFactory();

            Assert.Throws<ArgumentException>(() => readerFactory.GetReader(source));
        }

        [Fact]
        public void GetReader_Should_Throw_NotSupportedReaderSourceException()
        {
            string source = "test";
            var readerFactory = new ReaderFactory();

            Assert.Throws<NotSupportedReaderSourceException>(() => readerFactory.GetReader(source));
        }

        [Fact]
        public void GetReader_Should_Return_File_Reader()
        {
            string source = $"{Constants.DirectoryName}/test.yaml";
            var readerFactory = new ReaderFactory();

            var reader = readerFactory.GetReader(source);

            Assert.NotNull(reader);
            Assert.IsType<FileReader>(reader);
        }
    }
}