# SQL Test Assignment

Attached is a mysqldump of a database to be used during the test.

Below are the questions for this test. Please enter a full, complete, working SQL statement under each question. We do not want the answer to the question. We want the SQL command to derive the answer. We will copy/paste these commands to test the validity of the answer.

**Example:**

_Q. Select all users_

- Please return at least first_name and last_name

SELECT first_name, last_name FROM users;


------

**— Test Starts Here —**

1. Select users whose id is either 3,2 or 4
- Please return at least: all user fields
SELECT * FROM users  WHERE Id IN (2, 3, 4)

2. Count how many basic and premium listings each active user has
- Please return at least: first_name, last_name, basic, premium
SELECT 
	u.first_name,
	u.last_name,
	SUM(CASE WHEN l.status = 2 THEN 1 ELSE 0 END) AS basic,
	SUM(CASE WHEN l.status = 3 THEN 1 ELSE 0 END) AS premium
FROM users as u
INNER JOIN listings as l ON u.id = l.user_id
WHERE u.status = 2 AND l.status IN (2, 3)
GROUP BY u.id

3. Show the same count as before but only if they have at least ONE premium listing
- Please return at least: first_name, last_name, basic, premium
SELECT 
	u.first_name,
	u.last_name,
	SUM(CASE WHEN l.status = 2 THEN 1 ELSE 0 END) AS basic,
	SUM(CASE WHEN l.status = 3 THEN 1 ELSE 0 END) AS premium
FROM users as u
INNER JOIN listings as l ON u.id = l.user_id
WHERE u.status = 2 AND l.status IN (2, 3)
GROUP BY u.id
HAVING SUM(CASE WHEN l.status = 3 THEN 1 ELSE 0 END) > 0

4. How much revenue has each active vendor made in 2013
- Please return at least: first_name, last_name, currency, revenue
SELECT u.first_name, u.last_name, c.currency, SUM(c.price) as revenue
FROM users as u 
INNER JOIN listings as l ON u.id = l.user_id
INNER JOIN clicks as c ON c.listing_id = l.id
WHERE EXTRACT(YEAR FROM c.created) = 2013 AND u.status = 2
GROUP BY u.id, c.currency

5. Insert a new click for listing id 3, at $4.00
- Find out the id of this new click. Please return at least: id
INSERT INTO public.clicks(listing_id, price, currency, created) VALUES (3, 4.00, 'USD', CURRENT_TIMESTAMP)
RETURNING id;

6. Show listings that have not received a click in 2013
- Please return at least: listing_name
SELECT DISTINCT(name) as listing_name 
FROM listings 
WHERE id NOT IN (
	SELECT DISTINCT(l.id)
	FROM clicks as c
	INNER JOIN listings as l ON l.id = c.listing_id
	WHERE EXTRACT(YEAR FROM c.created) = 2013 
)

7. For each year show number of listings clicked and number of vendors who owned these listings
- Please return at least: date, total_listings_clicked, total_vendors_affected
SELECT 
	EXTRACT(YEAR FROM c.created) as date,
	COUNT(1) as total_listings_clicked,
	COUNT(DISTINCT(u.id)) as total_vendors_affected
FROM users as u
INNER JOIN listings as l ON u.id = l.user_id
INNER JOIN clicks as c ON l.id = c.listing_id
GROUP BY EXTRACT(YEAR FROM c.created)

8. Return a comma separated string of listing names for all active vendors
- Please return at least: first_name, last_name, listing_names
SELECT u.first_name, u.last_name, string_agg(l.name::text, ',') as listing_names
FROM users as u
INNER JOIN listings as l ON u.id = l.user_id
WHERE u.status = 2
GROUP BY u.id