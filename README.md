# Saas Product Uploader

## Usage

1. Run build command 
- You can run build.cmd or build.ps1 or build.sh (depending on the platform)
- This build script will run restore, compile, unit test, functional test and publish command. You can track the progress from console.

2. Then go to artifacts directory (cd ./artifacts) and open command line.

3. Test it, ex. import capterra feed-products/capterra.yaml

## Tools & Libraries

I used NUKE (https://nuke.build/) for build automation.
<br>
<br>
<br>

# Sql Test
 - You can find sql file with my answers under the sql directory.