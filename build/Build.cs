using System;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild
{
    public static int Main () => Execute<Build>(x => x.FunctionalTest);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = Configuration.Release;

    [Solution] readonly Solution Solution;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

    readonly string ProjectName = "ProductUploader.Cli";


    Target Restore => _ => _
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);

            DotNetRestore(s => s
                .SetProperty("UseSharedCompilation", false)
                .SetProjectFile(Solution));
        });

    Target UnitTest => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            Project unitTestProject = Solution.GetProject("UnitTests");

            DotNetTest(s => s
                .SetProjectFile(unitTestProject)
                .SetConfiguration(Configuration));
        });

    Target FunctionalTest => _ => _
        .DependsOn(Publish)
        .Executes(() =>
        {
            Project functionalTestProject = Solution.GetProject("FunctionalTests");

            DotNetTest(s => s
                .SetProjectFile(functionalTestProject)
                .SetConfiguration(Configuration));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .EnableNoRestore());
        });

    Target Publish => _ => _
        .DependsOn(UnitTest)
        .Executes(() =>
        {
            var outputDirectory = ArtifactsDirectory;
            DotNetPublish(s => s
                .SetProject(Solution.GetProject(ProjectName))
                .SetOutput(outputDirectory)
                .SetConfiguration(Configuration)
                .EnableNoRestore());

            EnsureExistingDirectory(outputDirectory);
        });

}
